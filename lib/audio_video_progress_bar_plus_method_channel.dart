import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'audio_video_progress_bar_plus_platform_interface.dart';

/// An implementation of [AudioVideoProgressBarPlusPlatform] that uses method channels.
class MethodChannelAudioVideoProgressBarPlus
    extends AudioVideoProgressBarPlusPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('audio_video_progress_bar_plus');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
