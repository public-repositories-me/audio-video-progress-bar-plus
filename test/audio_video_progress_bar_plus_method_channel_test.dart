import 'package:audio_video_progress_bar_plus/audio_video_progress_bar_plus_method_channel.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MethodChannelAudioVideoProgressBarPlus platform =
      MethodChannelAudioVideoProgressBarPlus();
  const MethodChannel channel = MethodChannel('audio_video_progress_bar_plus');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
